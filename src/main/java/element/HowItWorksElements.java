package element;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HowItWorksElements {

    @FindBy(css = "a[data-cy = \"how-it-works-cta\"")
    protected WebElement startBtn;

    @FindBy(css = "div._2wJ-F > a")
    protected WebElement skipBtn;

    private final WebDriver driver;

    public HowItWorksElements(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
