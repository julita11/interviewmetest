package page;

import element.PersonalDataElements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class PersonalDataPage extends PersonalDataElements {

    private WebDriver driver;

    public PersonalDataPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void waitForPageLoad() {
        HowItWorksPage howItWorksPage = new HowItWorksPage(driver);
        howItWorksPage.skipTutorialPages();

        waitForElementVisible(firstNameInput);
    }

    public void setName() {
        waitForPageLoad();

        firstNameInput.sendKeys("Example");
        lastNameInput.sendKeys("Person");
    }

    public void setProfilePicture() {
        addPictureBtn.sendKeys(
                Paths.get("src/main/resources/profile_picture.png")
                        .toAbsolutePath().toString());
        savePictureBtn.click();
    }

    public BufferedImage downloadPreviewPicture() throws IOException {
        waitForElementVisible(picturePreview);

        String src = picturePreview.getAttribute("src");
        BufferedImage image = ImageIO.read(new URL(src));
        File outputfile = new File("src/main/resources/profile_picture_new.png");
        ImageIO.write(image, "png", outputfile);

        return image;
    }

    private void waitForElementVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public String getNamePreviewText() {
//        waitForElementVisible(namePreview);

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until((ExpectedCondition<Boolean>) d -> !namePreview.getText().equals("Twoje imię"));

        return namePreview.getText();
    }
}
